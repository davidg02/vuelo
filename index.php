<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>🛩Check-In - Vuelo AA001🛩</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0.900;1,100;1,300;1,400;1,
    500;1,700;1,900&display=swap" rel="stylsheet">
    <link rel="stylesheet" href="assets/css/estilos.css">
</head>

<body>

    <main>

        <div class="contenedor__todo">

            <h3>Bienvenidos al vuelo AA001</h3>

            <p>Por favor ingrese todos los datos correspondiente</p>

            <form action="php/alta.php" method="POST" id="formulario__checkin">

                Nombre y Apellido: <input type="text" placeholder="Nombre Completo" name="nombre"><br><br>
                Codigo de reserva: <input type="text" placeholder="Codigo de reserva. Ej:QWDCBN" name="reserva"><br><br>
                Nº pasaporte: <input type="text" placeholder="Numero de pasaporte" name="pasaporte"><br><br>
                Telefono: <input type="text" placeholder="Contacto de emergencia (Telefono)" name="telefono"><br><br>
                E-mail: <input type="text" placeholder="Correo electronico" name= "correo"><br><br>
                <button>Finalizar Check-In</button>

            </form>

        </div>

    </main>

</body>

</html>